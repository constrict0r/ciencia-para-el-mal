# Configuration for Sphinx documentation builder.

import os
import sys

project = "ciencia-para-el-mal"
copyright = "2019, Ciencia para el mal"
author = "constrict0r"
version = "0.0.1"
release = "0.0.1"

sys.path.insert(0, os.path.abspath("../.."))

extensions = [
    "sphinxcontrib.restbuilder",
    "sphinxcontrib.globalsubs",
    "sphinx-prompt",
    "sphinx_substitution_extensions"
]

templates_path = ["_templates"]

exclude_patterns = []

html_static_path = ["_static"]

html_theme = "sphinx_rtd_theme"

master_doc = "index"

img_url_base = "https://gitlab.com/"

img_url_part = "/raw/master/img/"

img_url = img_url_base + author + "/" + project + img_url_part

ci_url_base = "https://gitlab.com/"

ci_url = ci_url_base + author + "/" + project + "/pipelines"

global_substitutions = {
    "AUTHOR_IMG": ".. image:: " + img_url +
    "/autor.png\n   :alt: autor",
    "AUTHOR_SLOGAN": "Ciencia para el mal.",
    "CI_BADGE": ".. image:: " + img_url_base + author + "/" +
    project + "/badges/master/pipeline.svg\n   :alt: pipeline",
    "CI_LINK":  "`Gitlab CI <" + ci_url + ">`_.",
    "REPO_LINK":  "`Repositorio Gitlab <https://gitlab.com/"
    + author + "/" + project + ">`_.",
    "PROJECT": project,
    "READTHEDOCS_BADGE": ".. image:: https://readthedocs.org/projects/"
    + project + "/badge\n   :alt: readthedocs",
    "READTHEDOCS_LINK": "`readthedocs <https://" + project +
    ".readthedocs.io/en/latest/>`_."
}

substitutions = [
    ("|AUTHOR|", author),
    ("|PROJECT|", project)
]
