Uso
-----------------------------------------------------------------------------

Para descargar la imagen de Docker, desde un terminal ejecutar:

.. substitution-code-block:: bash

 docker pull constrict0r/ciencia-para-el-mal:latest

Una vez descargada la imagen, crear el contenedor y ejecutarlo:

.. substitution-code-block:: bash

 docker run -v /home/usuario/jupyter/:/home/jovyan -p 8888:8888/tcp --name ciencia-para-el-mal -ti constrict0r/ciencia-para-el-mal:latest

Cuando el contenedor ya existe y está detenido, ejecutar el siguiente comando
para correrlo:

.. substitution-code-block:: bash

 docker start ciencia-para-el-mal -i

Para usar la interfaz gráfica, visite el URL
`http://127.0.0.1:8888 <http://127.0.0.1:8888>`_.

Si es necesario se puede acceder a la consola del contenedor desde una segunda
terminal, para ello desde otra terminal ejecutar:

.. substitution-code-block:: bash

 docker exec -it ciencia-para-el-mal /bin/bash

Para construir la imagen de Docker a partir del Dockerfile, ir a la raíz
del repositorio y ejecutar:

.. substitution-code-block:: bash

 docker build -t constrict0r/ciencia-para-el-mal:latest .