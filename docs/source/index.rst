ciencia-para-el-mal
=============================================================================

|CI_BADGE|

|READTHEDOCS_BADGE|

Laboratorios automatizados de matemática.

Documentación completa en |READTHEDOCS_LINK|.

Código fuente en |REPO_LINK|.

Contenidos
=============================================================================

.. toctree::

   descripción

   uso

   requerimientos

   compatibilidad

   licencia

   enlaces

   autor

