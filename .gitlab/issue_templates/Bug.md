### ¿Cuál es el comportamiento actual del bug?

(Qué sucede)

### ¿Cuál es el comportamiento esperado?

(Qué deberías ver)

### Pasos para reproducirlo.

(Cómo reproducir el issue - esto es muy importante)

### Bitácoras relevantes y/o capturas de pantalla.

(Pegue cualquier bitácora relevante - por favor use bloques de código (```) para dar formato a la salida de texto en terminal, bitácoras y código porque si no es difícil de leer.)

### Correcciones posibles

(Si puedes, enlace a la línea de código responsable del problema)

Épica: #x

/label ~bug
