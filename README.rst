
ciencia-para-el-mal
*******************

.. image:: https://gitlab.com/constrict0r/ciencia-para-el-mal/badges/master/pipeline.svg
   :alt: pipeline

.. image:: https://readthedocs.org/projects/ciencia-para-el-mal/badge
   :alt: readthedocs

.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/constrict0r%2Fciencia-para-el-mal/master?filepath=index.ipynb

Laboratorios automatizados de matemática.

Documentación completa en `readthedocs
<https://ciencia-para-el-mal.readthedocs.io/en/latest/>`_.

Código fuente en `Repositorio Gitlab
<https://gitlab.com/constrict0r/ciencia-para-el-mal>`_.


Contenidos
**********

* `Descripción <#Descripción>`_
* `Uso <#Uso>`_
* `Requerimientos <#Requerimientos>`_
* `Compatibilidad <#Compatibilidad>`_
* `Licencia <#Licencia>`_
* `Enlaces <#Enlaces>`_
* `Autor <#Autor>`_

Descripción
***********

Laboratorios automatizados de matemática.


Uso
***

Para descargar la imagen de Docker, desde un terminal ejecutar:

::

   docker pull constrict0r/ciencia-para-el-mal:latest

Una vez descargada la imagen, crear el contenedor y ejecutarlo:

::

   docker run -v /home/usuario/jupyter/:/home/jovyan -p 8888:8888/tcp --name ciencia-para-el-mal -ti constrict0r/ciencia-para-el-mal:latest

Cuando el contenedor ya existe y está detenido, ejecutar el siguiente
comando para correrlo:

::

   docker start ciencia-para-el-mal -i

Para usar la interfaz gráfica, visite el URL `http://127.0.0.1:8888
<http://127.0.0.1:8888>`_.

Si es necesario se puede acceder a la consola del contenedor desde una
segunda terminal, para ello desde otra terminal ejecutar:

::

   docker exec -it ciencia-para-el-mal /bin/bash

Para construir la imagen de Docker a partir del Dockerfile, ir a la
raíz del repositorio y ejecutar:

::

   docker build -t constrict0r/ciencia-para-el-mal:latest .


Requerimientos
**************

* Docker.

* Python 3.


Compatibilidad
**************

* Debian buster.

* Debian stretch.

* Raspbian stretch.

* Raspbian buster.

* Ubuntu xenial.

* Windows 10.


Licencia
********

GPL 3. Vea el archivo LICENSE para más detalles.


Enlaces
*******

`Repositorio Gitlab
<https://gitlab.com/constrict0r/ciencia-para-el-mal>`_

`Gitlab CI
<https://gitlab.com/constrict0r/ciencia-para-el-mal/pipelines>`_


Autor
*****

.. image:: https://gitlab.com//constrict0r///ciencia-para-el-mal//raw/master/img/autor.png
   :alt: autor

Ciencia para el mal.

